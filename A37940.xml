<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A37940">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>An ordinance of the Lords and Commons assembled in Parliament for every second Tuesday in the moneth to be a day of recreation for schollers, apprentices, and servants</title>
    <author>England and Wales. Parliament.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A37940 of text R34898 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing E1888). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A37940</idno>
    <idno type="STC">Wing E1888</idno>
    <idno type="STC">ESTC R34898</idno>
    <idno type="EEBO-CITATION">14906222</idno>
    <idno type="OCLC">ocm 14906222</idno>
    <idno type="VID">102835</idno>
    <idno type="PROQUESTGOID">2240949406</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A37940)</note>
    <note>Transcribed from: (Early English Books Online ; image set 102835)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1569:36)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>An ordinance of the Lords and Commons assembled in Parliament for every second Tuesday in the moneth to be a day of recreation for schollers, apprentices, and servants</title>
      <author>England and Wales. Parliament.</author>
     </titleStmt>
     <extent>1 broadside.</extent>
     <publicationStmt>
      <publisher>Printed at London for John Wright ...,</publisher>
      <pubPlace>[London] :</pubPlace>
      <date>1647.</date>
     </publicationStmt>
     <notesStmt>
      <note>At head of title: Die Veneris II Iunii 1647.</note>
      <note>"Die Veneris II Junii 1647. Ordered by the Lords assembled in Parliament that this ordinance be forthwith printed and published. Joh. Brown Cler. Parliamentorum."</note>
      <note>Reproduction of original in the Harvard University Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Great Britain -- History -- Civil War, 1642-1649.</term>
     <term>Great Britain -- Politics and government -- 1642-1649.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A37940</ep:tcp>
    <ep:estc> R34898</ep:estc>
    <ep:stc> (Wing E1888). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>Die Veneris 11 Iunii 1647. An ordinance of the Lords and Commons assembled in Parliament for every second Tuesday in the moneth to be a day</ep:title>
    <ep:author>England and Wales. Parliament</ep:author>
    <ep:publicationYear>1647</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>355</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-07</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-07</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2009-01</date>
    <label>Emma (Leeson) Huber</label>
        Sampled and proofread
      </change>
   <change>
    <date>2009-01</date>
    <label>Emma (Leeson) Huber</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2009-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A37940-e10">
  <body xml:id="A37940-e20">
   <pb facs="tcp:102835:1" xml:id="A37940-001-a"/>
   <div type="text" xml:id="A37940-e30">
    <opener xml:id="A37940-e40">
     <dateline xml:id="A37940-e50">
      <date xml:id="A37940-e60">
       <w lemma="n/a" pos="fla" xml:id="A37940-001-a-0010">Die</w>
       <w lemma="n/a" pos="fla" xml:id="A37940-001-a-0020">Veneris</w>
       <w lemma="11" pos="crd" xml:id="A37940-001-a-0030">11</w>
       <w lemma="n/a" pos="fla" xml:id="A37940-001-a-0040">Iunii</w>
       <w lemma="1647." pos="crd" xml:id="A37940-001-a-0050">1647.</w>
       <pc unit="sentence" xml:id="A37940-001-a-0060"/>
      </date>
     </dateline>
    </opener>
    <head xml:id="A37940-e70">
     <w lemma="a" pos="d" xml:id="A37940-001-a-0070">An</w>
     <w lemma="ordinance" pos="n1" xml:id="A37940-001-a-0080">Ordinance</w>
     <w lemma="of" pos="acp" xml:id="A37940-001-a-0090">of</w>
     <w lemma="the" pos="d" xml:id="A37940-001-a-0100">the</w>
     <w lemma="lord" pos="n2" xml:id="A37940-001-a-0110">Lords</w>
     <w lemma="and" pos="cc" xml:id="A37940-001-a-0120">and</w>
     <w lemma="commons" pos="n2" xml:id="A37940-001-a-0130">Commons</w>
     <w lemma="assemble" pos="vvn" xml:id="A37940-001-a-0140">assembled</w>
     <w lemma="in" pos="acp" xml:id="A37940-001-a-0150">in</w>
     <w lemma="parliament" pos="n1" xml:id="A37940-001-a-0160">Parliament</w>
     <w lemma="for" pos="acp" xml:id="A37940-001-a-0170">for</w>
     <w lemma="every" pos="d" xml:id="A37940-001-a-0180">every</w>
     <w lemma="second" pos="ord" xml:id="A37940-001-a-0190">second</w>
     <w lemma="Tuesday" pos="nn1" xml:id="A37940-001-a-0200">Tuesday</w>
     <w lemma="in" pos="acp" xml:id="A37940-001-a-0210">in</w>
     <w lemma="the" pos="d" xml:id="A37940-001-a-0220">the</w>
     <w lemma="month" pos="n1" reg="month" xml:id="A37940-001-a-0230">Moneth</w>
     <w lemma="to" pos="prt" xml:id="A37940-001-a-0240">to</w>
     <w lemma="be" pos="vvi" xml:id="A37940-001-a-0250">be</w>
     <w lemma="a" pos="d" xml:id="A37940-001-a-0260">a</w>
     <w lemma="day" pos="n1" xml:id="A37940-001-a-0270">day</w>
     <w lemma="of" pos="acp" xml:id="A37940-001-a-0280">of</w>
     <w lemma="recreation" pos="n1" xml:id="A37940-001-a-0290">Recreation</w>
     <w lemma="for" pos="acp" xml:id="A37940-001-a-0300">for</w>
     <w lemma="scholar" pos="n2" reg="scholars" xml:id="A37940-001-a-0310">Schollers</w>
     <pc xml:id="A37940-001-a-0320">,</pc>
     <w lemma="apprentice" pos="n2" xml:id="A37940-001-a-0330">Apprentices</w>
     <pc xml:id="A37940-001-a-0340">,</pc>
     <w lemma="and" pos="cc" xml:id="A37940-001-a-0350">and</w>
     <w lemma="servant" pos="n2" xml:id="A37940-001-a-0360">Servants</w>
     <pc unit="sentence" xml:id="A37940-001-a-0370">.</pc>
    </head>
    <p xml:id="A37940-e80">
     <w lemma="forasmuch" pos="av" rend="decorinit" xml:id="A37940-001-a-0380">FOrasmuch</w>
     <w lemma="as" pos="acp" xml:id="A37940-001-a-0390">as</w>
     <w lemma="the" pos="d" xml:id="A37940-001-a-0400">the</w>
     <w lemma="feast" pos="n2" xml:id="A37940-001-a-0410">Feasts</w>
     <w lemma="of" pos="acp" xml:id="A37940-001-a-0420">of</w>
     <w lemma="the" pos="d" xml:id="A37940-001-a-0430">the</w>
     <w lemma="nativity" pos="n1" xml:id="A37940-001-a-0440">Nativity</w>
     <w lemma="of" pos="acp" xml:id="A37940-001-a-0450">of</w>
     <w lemma="Christ" pos="nn1" xml:id="A37940-001-a-0460">Christ</w>
     <pc xml:id="A37940-001-a-0470">,</pc>
     <w lemma="Easter" pos="nn1" xml:id="A37940-001-a-0480">Easter</w>
     <pc xml:id="A37940-001-a-0490">,</pc>
     <w lemma="Whitsuntide" pos="nn1" reg="Whitsuntide" xml:id="A37940-001-a-0500">Whitsontide</w>
     <pc xml:id="A37940-001-a-0510">,</pc>
     <w lemma="and" pos="cc" xml:id="A37940-001-a-0520">and</w>
     <w lemma="other" pos="d" xml:id="A37940-001-a-0530">other</w>
     <w lemma="festival" pos="n2" xml:id="A37940-001-a-0540">Festivals</w>
     <w lemma="common" pos="av-j" xml:id="A37940-001-a-0550">commonly</w>
     <w lemma="call" pos="vvn" xml:id="A37940-001-a-0560">called</w>
     <w lemma="holiday" pos="n2" reg="holidays" xml:id="A37940-001-a-0570">Holy-dayes</w>
     <pc xml:id="A37940-001-a-0580">,</pc>
     <w lemma="have" pos="vvb" xml:id="A37940-001-a-0590">have</w>
     <w lemma="be" pos="vvn" reg="been" xml:id="A37940-001-a-0600">beene</w>
     <w lemma="heretofore" pos="av" xml:id="A37940-001-a-0610">heretofore</w>
     <w lemma="superstitious" pos="av-j" xml:id="A37940-001-a-0620">superstitiously</w>
     <w lemma="use" pos="vvn" xml:id="A37940-001-a-0630">used</w>
     <w lemma="and" pos="cc" xml:id="A37940-001-a-0640">and</w>
     <w lemma="observe" pos="vvn" xml:id="A37940-001-a-0650">observed</w>
     <pc xml:id="A37940-001-a-0660">,</pc>
     <w lemma="be" pos="vvb" reg="be" xml:id="A37940-001-a-0670">Bee</w>
     <w lemma="it" pos="pn" xml:id="A37940-001-a-0680">it</w>
     <w lemma="ordain" pos="vvd" xml:id="A37940-001-a-0690">Ordained</w>
     <w lemma="by" pos="acp" xml:id="A37940-001-a-0700">by</w>
     <w lemma="the" pos="d" xml:id="A37940-001-a-0710">the</w>
     <w lemma="lord" pos="n2" xml:id="A37940-001-a-0720">Lords</w>
     <w lemma="and" pos="cc" xml:id="A37940-001-a-0730">and</w>
     <w lemma="commons" pos="n2" xml:id="A37940-001-a-0740">Commons</w>
     <w lemma="in" pos="acp" xml:id="A37940-001-a-0750">in</w>
     <w lemma="parliament" pos="n1" xml:id="A37940-001-a-0760">Parliament</w>
     <w lemma="assemble" pos="vvn" xml:id="A37940-001-a-0770">assembled</w>
     <pc xml:id="A37940-001-a-0780">,</pc>
     <w lemma="that" pos="cs" xml:id="A37940-001-a-0790">That</w>
     <w lemma="the" pos="d" xml:id="A37940-001-a-0800">the</w>
     <w lemma="say" pos="j-vn" xml:id="A37940-001-a-0810">said</w>
     <w lemma="feast" pos="n2" xml:id="A37940-001-a-0820">Feasts</w>
     <w lemma="of" pos="acp" xml:id="A37940-001-a-0830">of</w>
     <w lemma="the" pos="d" xml:id="A37940-001-a-0840">the</w>
     <w lemma="nativity" pos="n1" xml:id="A37940-001-a-0850">Nativity</w>
     <w lemma="of" pos="acp" xml:id="A37940-001-a-0860">of</w>
     <w lemma="Christ" pos="nn1" xml:id="A37940-001-a-0870">Christ</w>
     <pc xml:id="A37940-001-a-0880">,</pc>
     <w lemma="Easter" pos="nn1" xml:id="A37940-001-a-0890">Easter</w>
     <pc xml:id="A37940-001-a-0900">,</pc>
     <w lemma="and" pos="cc" xml:id="A37940-001-a-0910">and</w>
     <w lemma="Whisontide" pos="nn1" xml:id="A37940-001-a-0920">Whisontide</w>
     <pc xml:id="A37940-001-a-0930">,</pc>
     <w lemma="and" pos="cc" xml:id="A37940-001-a-0940">and</w>
     <w lemma="all" pos="d" xml:id="A37940-001-a-0950">all</w>
     <w lemma="other" pos="d" xml:id="A37940-001-a-0960">other</w>
     <w lemma="festival" pos="n1" reg="festival" xml:id="A37940-001-a-0970">Festivall</w>
     <w lemma="day" pos="n2" reg="days" xml:id="A37940-001-a-0980">dayes</w>
     <pc xml:id="A37940-001-a-0990">,</pc>
     <w lemma="common" pos="av-j" xml:id="A37940-001-a-1000">commonly</w>
     <w lemma="call" pos="vvn" xml:id="A37940-001-a-1010">called</w>
     <w lemma="holiday" pos="n2" reg="holidays" xml:id="A37940-001-a-1020">Holy-dayes</w>
     <pc xml:id="A37940-001-a-1030">,</pc>
     <w lemma="be" pos="vvb" xml:id="A37940-001-a-1040">be</w>
     <w lemma="no" pos="avx-d" xml:id="A37940-001-a-1050">no</w>
     <w lemma="long" pos="avc-j" xml:id="A37940-001-a-1060">longer</w>
     <w lemma="observe" pos="vvn" xml:id="A37940-001-a-1070">observed</w>
     <w lemma="as" pos="acp" xml:id="A37940-001-a-1080">as</w>
     <w lemma="festival" pos="n2" xml:id="A37940-001-a-1090">Festivals</w>
     <w lemma="or" pos="cc" xml:id="A37940-001-a-1100">or</w>
     <w lemma="holiday" pos="n2" reg="holidays" xml:id="A37940-001-a-1110">Holy-dayes</w>
     <w lemma="within" pos="acp" xml:id="A37940-001-a-1120">within</w>
     <w lemma="this" pos="d" xml:id="A37940-001-a-1130">this</w>
     <w lemma="kingdom" pos="n1" reg="kingdom" xml:id="A37940-001-a-1140">Kingdome</w>
     <w lemma="of" pos="acp" xml:id="A37940-001-a-1150">of</w>
     <w lemma="England" pos="nn1" rend="hi" xml:id="A37940-001-a-1160">England</w>
     <w lemma="and" pos="cc" xml:id="A37940-001-a-1170">and</w>
     <w lemma="dominion" pos="n1" xml:id="A37940-001-a-1180">Dominion</w>
     <w lemma="of" pos="acp" xml:id="A37940-001-a-1190">of</w>
     <hi xml:id="A37940-e100">
      <w lemma="Wales" pos="nn1" xml:id="A37940-001-a-1200">Wales</w>
      <pc xml:id="A37940-001-a-1210">,</pc>
     </hi>
     <w lemma="any" pos="d" xml:id="A37940-001-a-1220">any</w>
     <w lemma="law" pos="n1" xml:id="A37940-001-a-1230">Law</w>
     <pc xml:id="A37940-001-a-1240">,</pc>
     <w lemma="statute" pos="n1" xml:id="A37940-001-a-1250">Statute</w>
     <pc xml:id="A37940-001-a-1260">,</pc>
     <w lemma="custom" pos="n1" reg="custom" xml:id="A37940-001-a-1270">Custome</w>
     <pc xml:id="A37940-001-a-1280">,</pc>
     <w lemma="constitution" pos="n1" xml:id="A37940-001-a-1290">Constitution</w>
     <pc xml:id="A37940-001-a-1300">,</pc>
     <w lemma="or" pos="cc" xml:id="A37940-001-a-1310">or</w>
     <w lemma="cannon" pos="n1" xml:id="A37940-001-a-1320">Cannon</w>
     <w lemma="to" pos="acp" xml:id="A37940-001-a-1330">to</w>
     <w lemma="the" pos="d" xml:id="A37940-001-a-1340">the</w>
     <w lemma="contrary" pos="j" xml:id="A37940-001-a-1350">contrary</w>
     <w lemma="in" pos="acp" xml:id="A37940-001-a-1360">in</w>
     <w lemma="any" pos="d" xml:id="A37940-001-a-1370">any</w>
     <w lemma="wise" pos="j" xml:id="A37940-001-a-1380">wise</w>
     <w lemma="notwithstanding" pos="acp" xml:id="A37940-001-a-1390">notwithstanding</w>
     <pc xml:id="A37940-001-a-1400">:</pc>
     <w lemma="and" pos="cc" xml:id="A37940-001-a-1410">And</w>
     <w lemma="to" pos="acp" xml:id="A37940-001-a-1420">to</w>
     <w lemma="the" pos="d" xml:id="A37940-001-a-1430">the</w>
     <w lemma="end" pos="n1" xml:id="A37940-001-a-1440">end</w>
     <w lemma="that" pos="cs" xml:id="A37940-001-a-1450">that</w>
     <w lemma="there" pos="av" xml:id="A37940-001-a-1460">there</w>
     <w lemma="may" pos="vmb" xml:id="A37940-001-a-1470">may</w>
     <w lemma="be" pos="vvi" xml:id="A37940-001-a-1480">be</w>
     <w lemma="a" pos="d" xml:id="A37940-001-a-1490">a</w>
     <w lemma="convenient" pos="j" xml:id="A37940-001-a-1500">convenient</w>
     <w lemma="time" pos="n1" xml:id="A37940-001-a-1510">time</w>
     <w lemma="allot" pos="vvn" xml:id="A37940-001-a-1520">allotted</w>
     <w lemma="to" pos="acp" xml:id="A37940-001-a-1530">to</w>
     <w lemma="scholar" pos="n2" reg="scholars" xml:id="A37940-001-a-1540">Schollers</w>
     <pc xml:id="A37940-001-a-1550">,</pc>
     <w lemma="apprentice" pos="n2" xml:id="A37940-001-a-1560">Apprentices</w>
     <pc xml:id="A37940-001-a-1570">,</pc>
     <w lemma="and" pos="cc" xml:id="A37940-001-a-1580">and</w>
     <w lemma="other" pos="d" xml:id="A37940-001-a-1590">other</w>
     <w lemma="servant" pos="n2" xml:id="A37940-001-a-1600">Servants</w>
     <w lemma="for" pos="acp" xml:id="A37940-001-a-1610">for</w>
     <w lemma="their" pos="po" xml:id="A37940-001-a-1620">their</w>
     <w lemma="recreation" pos="n1" xml:id="A37940-001-a-1630">Recreation</w>
     <pc xml:id="A37940-001-a-1640">,</pc>
     <w lemma="be" pos="vvb" xml:id="A37940-001-a-1650">be</w>
     <w lemma="it" pos="pn" xml:id="A37940-001-a-1660">it</w>
     <w lemma="ordain" pos="vvd" xml:id="A37940-001-a-1670">Ordained</w>
     <w lemma="by" pos="acp" xml:id="A37940-001-a-1680">by</w>
     <w lemma="the" pos="d" xml:id="A37940-001-a-1690">the</w>
     <w lemma="authority" pos="n1" xml:id="A37940-001-a-1700">Authority</w>
     <w lemma="aforesaid" pos="j" xml:id="A37940-001-a-1710">aforesaid</w>
     <pc xml:id="A37940-001-a-1720">,</pc>
     <w lemma="that" pos="cs" xml:id="A37940-001-a-1730">That</w>
     <w lemma="all" pos="d" xml:id="A37940-001-a-1740">all</w>
     <w lemma="scholar" pos="n2" reg="scholars" xml:id="A37940-001-a-1750">Schollers</w>
     <pc xml:id="A37940-001-a-1760">,</pc>
     <w lemma="apprentice" pos="n2" xml:id="A37940-001-a-1770">Apprentices</w>
     <pc xml:id="A37940-001-a-1780">,</pc>
     <w lemma="and" pos="cc" xml:id="A37940-001-a-1790">and</w>
     <w lemma="other" pos="d" xml:id="A37940-001-a-1800">other</w>
     <w lemma="servant" pos="n2" xml:id="A37940-001-a-1810">Servants</w>
     <w lemma="shall" pos="vmb" xml:id="A37940-001-a-1820">shall</w>
     <w lemma="have" pos="vvi" xml:id="A37940-001-a-1830">have</w>
     <w lemma="such" pos="d" xml:id="A37940-001-a-1840">such</w>
     <w lemma="convenient" pos="j" xml:id="A37940-001-a-1850">convenient</w>
     <w lemma="reasonable" pos="j" xml:id="A37940-001-a-1860">reasonable</w>
     <w lemma="recreation" pos="n1" xml:id="A37940-001-a-1870">Recreation</w>
     <w lemma="and" pos="cc" xml:id="A37940-001-a-1880">and</w>
     <w lemma="relaxation" pos="n1" xml:id="A37940-001-a-1890">Relaxation</w>
     <w lemma="from" pos="acp" xml:id="A37940-001-a-1900">from</w>
     <w lemma="their" pos="po" xml:id="A37940-001-a-1910">their</w>
     <w lemma="constant" pos="j" xml:id="A37940-001-a-1920">constant</w>
     <w lemma="and" pos="cc" xml:id="A37940-001-a-1930">and</w>
     <w lemma="ordinary" pos="j" xml:id="A37940-001-a-1940">ordinary</w>
     <w lemma="labour" pos="n2" xml:id="A37940-001-a-1950">Labours</w>
     <w lemma="on" pos="acp" xml:id="A37940-001-a-1960">on</w>
     <w lemma="every" pos="d" xml:id="A37940-001-a-1970">every</w>
     <w lemma="second" pos="ord" xml:id="A37940-001-a-1980">second</w>
     <w lemma="Tuesday" pos="nn1" xml:id="A37940-001-a-1990">Tuesday</w>
     <w lemma="in" pos="acp" xml:id="A37940-001-a-2000">in</w>
     <w lemma="the" pos="d" xml:id="A37940-001-a-2010">the</w>
     <w lemma="month" pos="n1" reg="month" xml:id="A37940-001-a-2020">Moneth</w>
     <w lemma="throughout" pos="acp" reg="throughout" xml:id="A37940-001-a-2030">thorowout</w>
     <w lemma="the" pos="d" xml:id="A37940-001-a-2040">the</w>
     <w lemma="year" pos="n1" reg="year" xml:id="A37940-001-a-2050">yeare</w>
     <pc xml:id="A37940-001-a-2060">,</pc>
     <w lemma="as" pos="acp" xml:id="A37940-001-a-2070">as</w>
     <w lemma="former" pos="av-j" xml:id="A37940-001-a-2080">formerly</w>
     <w lemma="they" pos="pns" xml:id="A37940-001-a-2090">they</w>
     <w lemma="have" pos="vvb" xml:id="A37940-001-a-2100">have</w>
     <w lemma="use" pos="vvn" xml:id="A37940-001-a-2110">used</w>
     <w lemma="to" pos="prt" xml:id="A37940-001-a-2120">to</w>
     <w lemma="have" pos="vvi" xml:id="A37940-001-a-2130">have</w>
     <w lemma="on" pos="acp" xml:id="A37940-001-a-2140">on</w>
     <w lemma="such" pos="d" xml:id="A37940-001-a-2150">such</w>
     <w lemma="aforesaid" pos="j" xml:id="A37940-001-a-2160">aforesaid</w>
     <w lemma="festival" pos="n2" xml:id="A37940-001-a-2170">Festivals</w>
     <pc xml:id="A37940-001-a-2180">,</pc>
     <w lemma="common" pos="av-j" xml:id="A37940-001-a-2190">commonly</w>
     <w lemma="call" pos="vvn" xml:id="A37940-001-a-2200">called</w>
     <w lemma="holiday" pos="n2" reg="holidays" xml:id="A37940-001-a-2210">Holy-dayes</w>
     <pc unit="sentence" xml:id="A37940-001-a-2220">.</pc>
     <w lemma="and" pos="cc" xml:id="A37940-001-a-2230">And</w>
     <w lemma="that" pos="d" xml:id="A37940-001-a-2240">that</w>
     <w lemma="master" pos="n2" xml:id="A37940-001-a-2250">Masters</w>
     <w lemma="of" pos="acp" xml:id="A37940-001-a-2260">of</w>
     <w lemma="all" pos="d" xml:id="A37940-001-a-2270">all</w>
     <w lemma="scholar" pos="n2" reg="scholars" xml:id="A37940-001-a-2280">Schollers</w>
     <pc xml:id="A37940-001-a-2290">,</pc>
     <w lemma="apprentice" pos="n2" xml:id="A37940-001-a-2300">Apprentices</w>
     <pc xml:id="A37940-001-a-2310">,</pc>
     <w lemma="and" pos="cc" xml:id="A37940-001-a-2320">and</w>
     <w lemma="servant" pos="n2" xml:id="A37940-001-a-2330">Servants</w>
     <w lemma="shall" pos="vmb" xml:id="A37940-001-a-2340">shall</w>
     <w lemma="grant" pos="vvi" xml:id="A37940-001-a-2350">grant</w>
     <w lemma="unto" pos="acp" xml:id="A37940-001-a-2360">unto</w>
     <w lemma="they" pos="pno" xml:id="A37940-001-a-2370">them</w>
     <w lemma="respective" pos="av-j" xml:id="A37940-001-a-2380">respectively</w>
     <w lemma="such" pos="d" xml:id="A37940-001-a-2390">such</w>
     <w lemma="time" pos="n1" xml:id="A37940-001-a-2400">time</w>
     <w lemma="for" pos="acp" xml:id="A37940-001-a-2410">for</w>
     <w lemma="their" pos="po" xml:id="A37940-001-a-2420">their</w>
     <w lemma="recreation" pos="n2" xml:id="A37940-001-a-2430">Recreations</w>
     <w lemma="on" pos="acp" xml:id="A37940-001-a-2440">on</w>
     <w lemma="the" pos="d" xml:id="A37940-001-a-2450">the</w>
     <w lemma="aforesaid" pos="j" xml:id="A37940-001-a-2460">aforesaid</w>
     <w lemma="second" pos="vvb" xml:id="A37940-001-a-2470">second</w>
     <w lemma="tuesday" pos="nn2" reg="Tuesdays" xml:id="A37940-001-a-2480">Tuesdayes</w>
     <w lemma="in" pos="acp" xml:id="A37940-001-a-2490">in</w>
     <w lemma="every" pos="d" xml:id="A37940-001-a-2500">every</w>
     <w lemma="month" pos="n1" reg="month" xml:id="A37940-001-a-2510">Moneth</w>
     <pc xml:id="A37940-001-a-2520">,</pc>
     <w lemma="as" pos="acp" xml:id="A37940-001-a-2530">as</w>
     <w lemma="they" pos="pns" xml:id="A37940-001-a-2540">they</w>
     <w lemma="may" pos="vmb" xml:id="A37940-001-a-2550">may</w>
     <w lemma="convenient" pos="av-j" xml:id="A37940-001-a-2560">conveniently</w>
     <w lemma="spare" pos="vvi" xml:id="A37940-001-a-2570">spare</w>
     <w lemma="from" pos="acp" xml:id="A37940-001-a-2580">from</w>
     <w lemma="their" pos="po" xml:id="A37940-001-a-2590">their</w>
     <w lemma="extraordinary" pos="j" xml:id="A37940-001-a-2600">extraordinary</w>
     <w lemma="and" pos="cc" xml:id="A37940-001-a-2610">and</w>
     <w lemma="necessary" pos="j" xml:id="A37940-001-a-2620">necessary</w>
     <w lemma="service" pos="n2" xml:id="A37940-001-a-2630">Services</w>
     <w lemma="and" pos="cc" xml:id="A37940-001-a-2640">and</w>
     <w lemma="occasion" pos="n2" xml:id="A37940-001-a-2650">Occasions</w>
     <pc unit="sentence" xml:id="A37940-001-a-2660">.</pc>
     <w lemma="and" pos="cc" xml:id="A37940-001-a-2670">And</w>
     <w lemma="it" pos="pn" xml:id="A37940-001-a-2680">it</w>
     <w lemma="be" pos="vvz" xml:id="A37940-001-a-2690">is</w>
     <w lemma="further" pos="jc" xml:id="A37940-001-a-2700">further</w>
     <w lemma="ordain" pos="vvn" xml:id="A37940-001-a-2710">Ordained</w>
     <w lemma="by" pos="acp" xml:id="A37940-001-a-2720">by</w>
     <w lemma="the" pos="d" xml:id="A37940-001-a-2730">the</w>
     <w lemma="say" pos="j-vn" xml:id="A37940-001-a-2740">said</w>
     <w lemma="lord" pos="n2" xml:id="A37940-001-a-2750">Lords</w>
     <w lemma="and" pos="cc" xml:id="A37940-001-a-2760">and</w>
     <w lemma="commons" pos="n2" xml:id="A37940-001-a-2770">Commons</w>
     <pc xml:id="A37940-001-a-2780">,</pc>
     <w lemma="that" pos="cs" xml:id="A37940-001-a-2790">that</w>
     <w lemma="if" pos="cs" xml:id="A37940-001-a-2800">if</w>
     <w lemma="any" pos="d" xml:id="A37940-001-a-2810">any</w>
     <w lemma="difference" pos="n1" xml:id="A37940-001-a-2820">difference</w>
     <w lemma="shall" pos="vmb" xml:id="A37940-001-a-2830">shall</w>
     <w lemma="arise" pos="vvi" xml:id="A37940-001-a-2840">arise</w>
     <w lemma="between" pos="acp" reg="between" xml:id="A37940-001-a-2850">betweene</w>
     <w lemma="any" pos="d" xml:id="A37940-001-a-2860">any</w>
     <w lemma="master" pos="n1" xml:id="A37940-001-a-2870">Master</w>
     <w lemma="and" pos="cc" xml:id="A37940-001-a-2880">and</w>
     <w lemma="servant" pos="n1" xml:id="A37940-001-a-2890">Servant</w>
     <w lemma="concerning" pos="acp" xml:id="A37940-001-a-2900">concerning</w>
     <w lemma="the" pos="d" xml:id="A37940-001-a-2910">the</w>
     <w lemma="liberty" pos="n1" xml:id="A37940-001-a-2920">Liberty</w>
     <w lemma="hereby" pos="av" xml:id="A37940-001-a-2930">hereby</w>
     <w lemma="grant" pos="vvn" xml:id="A37940-001-a-2940">granted</w>
     <pc xml:id="A37940-001-a-2950">,</pc>
     <w lemma="the" pos="d" xml:id="A37940-001-a-2960">the</w>
     <w lemma="next" pos="ord" xml:id="A37940-001-a-2970">next</w>
     <w lemma="justice" pos="n1" xml:id="A37940-001-a-2980">Justice</w>
     <w lemma="of" pos="acp" xml:id="A37940-001-a-2990">of</w>
     <w lemma="peace" pos="n1" xml:id="A37940-001-a-3000">Peace</w>
     <w lemma="shall" pos="vmb" xml:id="A37940-001-a-3010">shall</w>
     <w lemma="have" pos="vvi" xml:id="A37940-001-a-3020">have</w>
     <w lemma="power" pos="n1" xml:id="A37940-001-a-3030">power</w>
     <w lemma="to" pos="prt" xml:id="A37940-001-a-3040">to</w>
     <w lemma="hear" pos="vvi" reg="hear" xml:id="A37940-001-a-3050">heare</w>
     <w lemma="and" pos="cc" xml:id="A37940-001-a-3060">and</w>
     <w lemma="determine" pos="vvi" xml:id="A37940-001-a-3070">determine</w>
     <w lemma="the" pos="d" xml:id="A37940-001-a-3080">the</w>
     <w lemma="same" pos="d" xml:id="A37940-001-a-3090">same</w>
     <pc unit="sentence" xml:id="A37940-001-a-3100">.</pc>
    </p>
   </div>
  </body>
  <back xml:id="A37940-e110">
   <div type="license" xml:id="A37940-e120">
    <opener xml:id="A37940-e130">
     <dateline xml:id="A37940-e140">
      <date xml:id="A37940-e150">
       <w lemma="n/a" pos="fla" xml:id="A37940-001-a-3110">Die</w>
       <w lemma="n/a" pos="fla" xml:id="A37940-001-a-3120">Veneris</w>
       <w lemma="11" pos="crd" xml:id="A37940-001-a-3130">11</w>
       <w lemma="n/a" pos="fla" xml:id="A37940-001-a-3140">Junii</w>
       <w lemma="1647." pos="crd" xml:id="A37940-001-a-3150">1647.</w>
       <pc unit="sentence" xml:id="A37940-001-a-3160"/>
      </date>
     </dateline>
    </opener>
    <p xml:id="A37940-e160">
     <w lemma="order" pos="j-vn" xml:id="A37940-001-a-3170">ORdered</w>
     <w lemma="by" pos="acp" xml:id="A37940-001-a-3180">by</w>
     <w lemma="the" pos="d" xml:id="A37940-001-a-3190">the</w>
     <w lemma="lord" pos="n2" xml:id="A37940-001-a-3200">Lords</w>
     <w lemma="assemble" pos="vvn" xml:id="A37940-001-a-3210">assembled</w>
     <w lemma="in" pos="acp" xml:id="A37940-001-a-3220">in</w>
     <w lemma="parliament" pos="n1" xml:id="A37940-001-a-3230">Parliament</w>
     <pc xml:id="A37940-001-a-3240">,</pc>
     <w lemma="that" pos="cs" xml:id="A37940-001-a-3250">That</w>
     <w lemma="this" pos="d" xml:id="A37940-001-a-3260">this</w>
     <w lemma="ordinance" pos="n1" xml:id="A37940-001-a-3270">Ordinance</w>
     <w lemma="be" pos="vvb" xml:id="A37940-001-a-3280">be</w>
     <w lemma="forthwith" pos="av" xml:id="A37940-001-a-3290">forthwith</w>
     <w lemma="print" pos="vvn" xml:id="A37940-001-a-3300">printed</w>
     <w lemma="and" pos="cc" xml:id="A37940-001-a-3310">and</w>
     <w lemma="publish" pos="vvn" xml:id="A37940-001-a-3320">published</w>
     <pc unit="sentence" xml:id="A37940-001-a-3330">.</pc>
    </p>
    <closer xml:id="A37940-e170">
     <signed xml:id="A37940-e180">
      <w lemma="joh." pos="ab" xml:id="A37940-001-a-3340">Joh.</w>
      <w lemma="Brown" pos="nn1" xml:id="A37940-001-a-3350">Brown</w>
      <w lemma="cler." pos="ab" xml:id="A37940-001-a-3360">Cler.</w>
      <w lemma="n/a" pos="fla" xml:id="A37940-001-a-3370">Parliamentorum</w>
      <pc unit="sentence" xml:id="A37940-001-a-3380">.</pc>
     </signed>
    </closer>
   </div>
   <div type="colophon" xml:id="A37940-e190">
    <p xml:id="A37940-e200">
     <w lemma="print" pos="vvn" xml:id="A37940-001-a-3390">Printed</w>
     <w lemma="at" pos="acp" xml:id="A37940-001-a-3400">at</w>
     <w lemma="London" pos="nn1" rend="hi" xml:id="A37940-001-a-3410">London</w>
     <w lemma="for" pos="acp" xml:id="A37940-001-a-3420">for</w>
     <hi xml:id="A37940-e220">
      <w lemma="john" pos="nn1" xml:id="A37940-001-a-3430">John</w>
      <w lemma="wright" pos="nn1" xml:id="A37940-001-a-3440">Wright</w>
     </hi>
     <w lemma="at" pos="acp" xml:id="A37940-001-a-3450">at</w>
     <w lemma="the" pos="d" xml:id="A37940-001-a-3460">the</w>
     <w lemma="king" pos="ng1" reg="King's" xml:id="A37940-001-a-3470">Kings</w>
     <w lemma="head" pos="n1" xml:id="A37940-001-a-3480">Head</w>
     <w lemma="in" pos="acp" xml:id="A37940-001-a-3490">in</w>
     <w lemma="the" pos="d" xml:id="A37940-001-a-3500">the</w>
     <w lemma="old" pos="j" xml:id="A37940-001-a-3510">old</w>
     <w lemma="Bayley" pos="nn1" xml:id="A37940-001-a-3520">Bayley</w>
     <pc unit="sentence" xml:id="A37940-001-a-3530">.</pc>
     <w lemma="1647." pos="crd" xml:id="A37940-001-a-3540">1647.</w>
     <pc unit="sentence" xml:id="A37940-001-a-3550"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
